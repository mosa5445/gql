const users = [
    {
        id: 1,
        name: "Ali",
        email: "ali@example.com",
        age: 24,
        sex: "male"
    },
    {
        id: 2,
        name: 'sara',
        email: "sara@example.com",
        age: 12,
        sex: "female"
    },
    {
        id: 3,
        name: "mohammad",
        email: "mosa5445@gmail.com",
        age: 20,
        sex: "male"
    },
    {
        id: 4,
        name: "sogand",
        email: "sogand@example.com",
        age: 23,
    }
]

const posts = [
    {
        id: 1,
        title: "چگونه برنامه نویس خوبی باشیم",
        content: "یک برنامه نویس خوب باید بداند که  ...",
        published: true,
        author: 1
    },
    {
        id: 2,
        title: "محبوب زبان برنامه نویسی سال 1398",
        content: "پایتون به عنوان محبوب ترین زبان برنامه نویسی سال 2019 ...",
        published: true,
        author: 1
    },
    {
        id: 3,
        title: "با جاوا اسکریپت آشنا شوید",
        content: "زبان برنامه نویسی محبوب جاوااسکریپت که ...",
        published: true,
        author: 3
    },
    {
        id: 4,
        title: "بهترین منابع یاد گیری برنامه نویسی",
        content: "تمام تازه کار ها به دنبال ...",
        published: true,
        author: 3
    }
]


const comments = [
    {
        id: 1,
        message: "مطلب خوبی بود",
        author: 2,
        post: 2,
    },
    {
        id: 2,
        message: "اسکول",
        author: 3,
        post: 2,
        parent: 1
    },
    {
        id: 3,
        message: "دمت گرم مشتی",
        author: 1,
        post: 2
    },
    {
        id: 4,
        message: "حلالت به مولا",
        author: 3,
        post: 2
    }
]

const data = {
    users,
    posts,
    comments
}

export { data as default  }