import { GraphQLServer, PubSub } from 'graphql-yoga'
import Comment from './resolvers/Comment'
import Mutation from './resolvers/Mutation'
import Subscription from './resolvers/Subscription'
import Post from './resolvers/Post'
import Query from './resolvers/Query'
import User from './resolvers/User'
import db from './db'

const pubsub = new PubSub()

const server = new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers: {
        Query,
        Mutation,
        Subscription,
        User,
        Post,
        Comment
    },
    context: {
        db,
        pubsub
    }
})

server.start(() => {
    console.log('server started')
})