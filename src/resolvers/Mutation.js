import uuid4 from 'uuid/v4'

const Mutation = {
    createComment(parent, args, { db, pubsub }, info) {
        args = args.body
        const userExist = db.users.some((user) => user.id == args.author)
        if (!userExist)
            throw new Error("invalid user id")

        const postExist = db.posts.some((post) => post.id == args.post)
        if (!postExist)
            throw new Error("invalid post id")
        if (args.parent) {
            const parentExist = db.comments.some((comment) => comment.id == args.parent)
            if (!parentExist)
                throw new Error("invalid comment id for parnt")
        }
        const comment = {
            id: uuid4(),
            ...args
        }

        db.comments.push(comment)
        pubsub.publish(`comments of ${args.post}`, { comment })

        return comment
    }
}

export { Mutation as default }