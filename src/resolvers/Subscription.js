import { setInterval } from "timers"

const Subscription = {
    comment: {
        subscribe(parent, { postId }, { db, pubsub }, info) {
            const post = db.posts.find((post) => post.id == postId)

            if (!post)
                throw new Error('post not found!')
            return pubsub.asyncIterator(`comments of ${postId}`)
        }
    }
}

export { Subscription as default }