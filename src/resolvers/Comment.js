const Comment = {
    childs(parent, args, { db }, info) {
        return db.comments.filter((comment) => {
            return comment.parent == parent.id
        })
    },
    post(parent, args, { db }, info) {
        return db.posts.find((post) => post.id == parent.post)
    },
    author(parent, args, { db }, info) {
        return db.users.find((user) => {
            return user.id == parent.author
        })
    }
}

export { Comment as default }